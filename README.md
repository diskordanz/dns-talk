# DNS talk

This repo contains slides to my introductory talk on the domain name system (DNS). Built using [Inkscape](https://inkscape.org/) and [Sozi](https://sozi.baierouge.fr/).

The slides are served here: [diskordanz.gitlab.io/dns-talk/](https://diskordanz.gitlab.io/dns-talk/)
